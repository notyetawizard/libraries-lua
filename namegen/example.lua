namegen = require "namegen"
math.randomseed(os.time())

firstnames = namegen.newNameList("namelist-first.txt")
firstnames.limit = 6

print(firstnames:remaining() .. " usable first names exist!")

lastnames = namegen.newNameList("namelist-last.txt")
lastnames.limit = 2

print(lastnames:remaining() .. " usable last names exist!")

fullnames = namegen.newNameGen(firstnames, lastnames)
fullnames.list.limit = 1

print(fullnames:remaining() .. " possible fullnames exist!")

local fullname, names = fullnames:gen()
local i = 1
while fullname ~= nil do
	print(i, fullname)
	print("\t"..firstnames.used[names[1]] .. " " .. lastnames.used[names[2]])

	fullname, names = fullnames:gen()
	i = i+1
end
