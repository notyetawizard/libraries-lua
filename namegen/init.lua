local namegen = {}

function namegen.newNameList(...)
-- Create a new NameList object
-- Can be passed multiple file names,
-- and will combine the files into a single list.

-- Reads names line by line from files.
-- Names may include *any* characters, I think.
	local NameList = {
		used = {},
		limit = 1,
		count = 0
	}
	
	function NameList:add(...)
	-- Add a new name to the list.
	-- Multiple names may be passed.
		local args = {...}
		for i, arg in ipairs(args) do
			if type(arg) == "string" then
				table.insert(self, arg)
				self.used[arg] = 0
			end
		end
	end
	
	function NameList:rm(...)
	-- Remove a name from the list
	-- Multiple names may be passed.
		local args = {...}
		for i, arg in ipairs(args) do
			if type(arg) == "string" then
				for k, name in ipairs(self) do
					if name == arg then table.remove(self, k) end
				end
				self.count = self.count - self.used[arg]
			end
		end
	end
	
	function NameList:use(...)
    -- Increment the usage count for a name by one,
	-- Multiple names may be passed, each followed by a number
	-- of times to use the name, which may be negative.

	-- This will add a name to the namelist if it doesn't exist,
	-- or remove a name with a usage below 0 (even after adding it!).		
		if not ... then return end
		local args = {...}
		for i, arg in ipairs(args) do
		
			if type(arg) == "string" then
				if not self.used[arg] then self:add(arg) end
				local uses = 1
				if type(args[i+1]) == "number" then uses = args[i+1] end
				self.used[arg] = self.used[arg] + uses
				self.count = self.count + uses
				if self.used[arg] < 0 then self:rm(arg) end
			end
		end
	end
	
	function NameList:remaining()
	-- Returns the number of names remaining,
	-- accounting for the set limit.
		return (#self * self.limit) - self.count
	end
	
	function NameList:get()
	-- Gets a random name from the list, respecting set limit,
	-- and increments the count accordingly.
		if self:remaining() <= 0 then return end
		local name = self[math.random(#self)]
		while self.used[name] >= self.limit do
			name = self[math.random(#self)]
		end
		self:use(name)
		return name
	end
	
	-- Add all the names to the list, if there are any!
	local args = {...}
	for i, filename in ipairs(args) do
		local file = io.open(filename, "r")
		if file then
			local name = file:read("*l")
			while name ~= nil do
				NameList:add(name)
				name = file:read("*l")
			end
			file:close()
		end
	end
	return NameList
end

function namegen.newNameGen(...)
-- Create a new NameGen object.
-- Can be passed multiple NameList objects,
-- and will generate new names from them in order.

-- e.g. namegen:newNameGen(firstnamelist, lastnamelist)
	NameGen = {
		list = namegen.newNameList(),
	}
	
	function NameGen:remaining()
		local r = 1
		for _, namelist in ipairs(self) do
			r = r * #namelist
		end
		return (r * self.list.limit) - self.list.count
	end
	
	function NameGen:gen()
		if NameGen:remaining() <= 0 then return end
		
		local function getNames()
			local names = {}
			for i, namelist in ipairs(self) do
				if namelist:remaining() <= 0 then return nil end
				local name = namelist:get()
				table.insert(names, name)
			end
			local fullname = table.concat(names, " ")
			return fullname, names
		end
	
		local fullname, names = getNames()
		while self.list.used[fullname]
		and self.list.used[fullname] >= self.list.limit do
			for n, name in ipairs(names) do
				self[n]:use(name, -1)
			end
			fullname, names = getNames()
		end
		self.list:use(fullname)
		return fullname, names
	end
	
	local args = {...}
	for _, namelist in ipairs(args) do
		table.insert(NameGen, namelist)
	end
	return NameGen
end

return namegen
