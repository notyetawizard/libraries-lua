data = {
	table1 = {
		[1] = 2,
		[2] = 3,
		[3] = 4,
		[4] = {
			[1] = 5,
			[2] = 6,
			this = "that",
			more = "stuff"
		},
		string2 = "Still scribing",
		string1 = "Hello World"
	},
	-- WARNING: Scribe doesn't handle the type "userdata"! Written as nil!
	fail3 = nil,
	-- WARNING: Scribe doesn't handle the type "function"! Written as nil!
	fail1 = nil,
	[12] = false,
	string3 = "going"
}
