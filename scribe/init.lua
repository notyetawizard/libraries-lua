local scribe = {}

function scribe:open(filename)
	self.file = io.open(filename, "w+")
	self.indent = 0
end

function scribe:close()
	self.file:close()
end

local function indent(n)
	return string.rep("\t", n)
end

function scribe:table(name, thing, comma)
	self.file:write(indent(self.indent) .. name .. " = {\n")
	self.indent = self.indent + 1
	for k, v in pairs(thing) do
		if not next(thing, k) then
			self:write(k, v)
		else self:write(k, v, true)
		end
	end
	self.indent = self.indent - 1
	if comma == true then self.file:write(indent(self.indent) .. "},\n")
	else self.file:write(indent(self.indent) .. "}\n")
	end
end

local escape_pattern = "[\a\b\f\n\r\t\v\\\"\']"

local escape_repl = {
	["\a"] = "\\a",
	["\b"] = "\\b",
	["\f"] = "\\f",
	["\n"] = "\\n",
	["\r"] = "\\r",
	["\t"] = "\\t",
	["\v"] = "\\v",
	["\\"] = "\\\\",
	["\""] = "\\\"",
	["\'"] = "\\\'"
}



function scribe:string(name, thing, comma)
	self.file:write(indent(self.indent) .. name .. " = \"" .. thing:gsub(escape_pattern, escape_repl) .. "\"")
	if comma == true then self.file:write(",\n")
	else self.file:write("\n")
	end
end


local function scribe_simple(self, name, thing, comma)
	self.file:write(indent(self.indent) .. name .. " = " .. tostring(thing))
	if comma == true then self.file:write(",\n")
	else self.file:write("\n")
	end
end

scribe.number = scribe_simple
scribe.boolean = scribe_simple

local function scribe_failure(self, name, thing, comma)
	local warning = "WARNING: Scribe doesn't handle the type \"" .. type(thing) .. "\"! Written as nil!\n"
	self.file:write(indent(self.indent) .. "-- " .. warning)
	self.file:write(indent(self.indent) .. name .. " = nil")
	if comma == true then self.file:write(",\n")
	else self.file:write("\n")
	end
end

scribe["function"] = scribe_failure
scribe["thread"] = scribe_failure
scribe["userdata"] = scribe_failure
scribe["nil"] = scribe_failure

function scribe:write(name, thing, comma)
	if type(name) == "number" then
		name = "["..name.."]"
	elseif tonumber(string.sub(name, 1, 1)) then
		name = "[\""..name.."\"]"
	end
	self[type(thing)](self, name, thing, comma)
end

return scribe
