local scribe = require "scribe"

scribe:open("example-output.lua")

local data = {
	table1 = {
		string1 = "Hello World",
		string2 = "Still scribing",
		2,
		3,
		4,
		{	more = "stuff",
			this = "that",
			5,
			6
		}
	},
	string3 = "going",
	[12] = false,
	fail1 = function () return end,
	fail2 = nil,
	fail3 = scribe.file
}

scribe:write("data", data)
scribe:close()

local loaddata = require "example-output"
